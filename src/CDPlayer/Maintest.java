/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDPlayer;

import java.util.Scanner;

/**
 *
 * @author Daniel
 */
public class Maintest {
    
    
    public static void main(String[] args) {
    
        System.out.println("laku");
        Song biisi1 = new Song(1100,"Biisi1");
        Song biisi2 = new Song(1200,"Biisi2");
        Song biisi3 = new Song(1300,"Biisi3");
        Song biisi4 = new Song(1400,"Biisi4");
        CD seedee = new CD(biisi1,biisi2,biisi3,biisi4,"musaseedee");
        CDPlayer musakone = new CDPlayer(seedee);
        
        Song biisi8 = new Song(1100,"Biisi5");
        Song biisi5 = new Song(1200,"Biisi4");
        Song biisi6 = new Song(1300,"Biisi6");
        Song biisi7 = new Song(1400,"Biisi7");
        CD seedee2 = new CD(biisi1,biisi2,biisi3,biisi4,"musaseedee2");
        
        
        Scanner reader = new Scanner(System.in);
        
    
    
    
        
            String input = "";
            while(!input.equals("exit")){  
            if(musakone.getPower()){
                System.out.println("\nCD Player Menu");
                System.out.println("1.Eject CD");
                System.out.println("2.Switch CD");
                System.out.println("3.Play / Pause");
                System.out.println("4.Forward");
                System.out.println("5.Previous"); 
                System.out.println("6.Power Off");
                System.out.println("\n\"exit\" to exit program");
                input = reader.nextLine();

                switch(input){
                    case"1":    System.out.println("Ejecting CD");
                                musakone.ejectCD();
                                break;
                    case"2":    System.out.println("WIP");
                                musakone.insertCD(seedee2);
                                break;
                    case"3":    musakone.playPause();
                                break;
                    case"4":    musakone.nextSong();
                                break;
                    case"5":    musakone.previousSong();
                                break;
                    case"6":    musakone.togglePower(false);
                                break;
                    case"exit": System.exit(0);
                                break;
                            }
                        }
            else{
                System.out.println("System not on. \"ON\" to switch on");
                while(!reader.nextLine().equalsIgnoreCase("on")){
                    
                }
                musakone.togglePower(true);
            }
    }
        
        
    }
    
}
