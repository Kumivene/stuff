/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDPlayer;

import java.util.Scanner;
/**
 *
 * @author Daniel
 */
public class CDPlayer implements Runnable{
    
    private boolean powerOn;
    private Song currentSong;
    private CD currentCD;
    private boolean shuffle;
    private boolean loop;
    private int currentTrack;
    volatile boolean keepPlaying = true;
    Scanner reader;
    
    
    public CDPlayer(CD newcd){
        
        this.powerOn = false;
        this.shuffle = false;
        this.loop = false;
        this.currentCD = newcd;
        this.currentSong = null;
        this.currentTrack = 0;
        reader = new Scanner(System.in);
        
    
    }
    
    public void run(){ //run method for pause method thread
        
        int playtime = 0;
        while (keepPlaying) {
            System.out.println("Playing song "+currentSongName()+" from CD: "+getCurrentCD());
            try {
                Thread.sleep(2000);
                playtime++;
                if (playtime >= 7){
                    nextSong();
                    playtime = 0;
                }
            } catch (InterruptedException e) {
            }
        }
        System.out.println("Paused");
        
    }
    
    public void togglePower(boolean h){
        this.powerOn=h;
    }
    
    public void playPause(){
        
        if(this.currentCD==null){
            System.out.println("No CD detected");
        }
        else{
            System.out.println("!!--Type pause to pause song--!!");
            System.out.println("Type next for next song");
            System.out.println("Type prev for previous song");
            
            //create new CDPlayer and run as thread
            
            CDPlayer pleijeri = new CDPlayer(this.currentCD);     
            String playInput = "";
            Thread t = new Thread(pleijeri);
            t.start();
            pleijeri.keepPlaying = true;
            pleijeri.currentTrack = this.currentTrack;//switch new CDPlayer currenTrack with the correct one

            //while(!reader.nextLine().equalsIgnoreCase("pause")){
            while(!playInput.equals("pause")){//check for keywords for interrutping thread / next prev songs
                
                if(playInput.equals("next")){
                    pleijeri.nextSong();
                    nextSong();
                    playInput = reader.nextLine();
                }
                else if(playInput.equals("prev")){
                    pleijeri.previousSong();
                    previousSong();
                    playInput = reader.nextLine();
                }
                else{playInput = reader.nextLine();}
            }
            pleijeri.keepPlaying = false; //set to false to stop run method while loop
            this.currentTrack = pleijeri.currentTrack;//update actual currentTrack with the one from temporary CDPlayer thread
            t.interrupt();//kill thread (pausing would be better)
        }
    }
    
    public String previousSong(){
        this.currentTrack--;
        if(this.currentTrack<0){
            this.currentTrack=3;
        }
            
        return this.currentCD.getSong(currentTrack);
    }
    
    public String nextSong(){
        this.currentTrack++;
        if(this.currentTrack>3){
            this.currentTrack=0;
        }
        return this.currentCD.getSong(currentTrack);
        
    }
    
    public void ejectCD(){
        this.currentCD=null;
    
}
    
    public void insertCD(CD newcd){
        
        
    }
    
    public String getCurrentCD(){
        return currentCD.getCDName();
        
    }
    
    public String currentSongName(){
        return currentCD.getSong(currentTrack);
        
    }
    
    public int currentSongDuration(){
        return currentCD.getSongObj(currentTrack).getDuratino();
        
    }
    
    public boolean getPower(){
        return this.powerOn;
        
    }}

