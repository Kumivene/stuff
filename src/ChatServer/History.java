import java.net.Socket;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;


public class History {
    ArrayList<String> history;
    ArrayList<User> users;
    Date date;
    
    public History(){
        history = new ArrayList<>();
        users = new ArrayList<>();
        history.add("Welcome to the Gonacave!");//default message to avoid errors
    }
    public void addUser(User u){
        this.users.add(u);
    }
    
    public void addHistory(String s, String nickname){//print message from one user to all users (observer pattern)
        date = new Date();//take date of moment the message is sent
        this.history.add("<" + date.toString() + ">" + " [" + nickname + "]" + ": " + s + "\r");//add message to history + date + user + \r to make telnet cleaner
        System.out.println(ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME));
        for(int i = 0;i < users.size();i++){
            this.users.get(i).printMessage("<" + date.toString() + ">" + " [" + nickname + "]" + ": " + s + "\r");//print message to a user
            System.out.println(date.toString() + ": " +s); // print message to console for debugging
        }
        
    }
    
    public ArrayList<User> getUsersHistory(){
        
        return users;
    }
    
    public ArrayList<String> getHistory(){
        return history;
    }
    public String latestHistory(){
        return history.get(history.size()-1);
    }
    public void removeUser(Socket s){//method checks for users in the userlist and removes the one given
        for(int i = 0; i<users.size();i++){
            if(users.get(i).getSocket().equals(s)){
                users.remove(i);
            }
        }
    }
    
}