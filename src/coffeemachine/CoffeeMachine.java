/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeemachine;

/**
 *
 * @author Daniel
 */
public class CoffeeMachine {
    
    private int beansCount;
    private int waterLevel;
    private boolean onOff;
    private int cleanInterval;
    
    
    public CoffeeMachine(){
        this.beansCount = 0;
        this.waterLevel = 0;
        this.onOff = false;
        this.cleanInterval = 0;
        
        
    }
    
    public void rinse(){
        if(this.waterLevel>=25){
            System.out.println("Rinsing!");
        }
        else{System.out.println("Could not rinse! Not enough water!");}
    }
    
    public void toggleOnOff(){
        
        this.onOff = !this.onOff;
        rinse();
}
    
    public void fillBeans(){
        
        this.beansCount = 200;
        System.out.println("Beans at 200g");
    }
    
    public void fillWater(){
        
        this.waterLevel = 1000;
        System.out.println("Water at 1000ml");
    }
    
    public void cleanMachine(){
        if(onOff){
            if(this.waterLevel>=300){
                this.cleanInterval = 0;
                System.out.println("Machine is clean!");
                this.waterLevel = this.waterLevel-300;
            }
            else{
                System.out.println("Not enough water!");
            }
        }
        else{}
    }
    
    public void espresso(){
        if(onOff){
            if(this.waterLevel>=30){
                if(this.cleanInterval<10){
                    this.beansCount = this.beansCount-8;
                    this.waterLevel = this.waterLevel-30;
                    this.cleanInterval++;
                    System.out.println("Making an espresso!");
                }
                else{
                    System.out.println("Run cleaning program before proceeding!");
                }
            }
            else{
                System.out.println("Not enough water!");
            }
        }
        else{}
    }
    
    public void blackcoffee(){
        if(onOff){
            if(this.waterLevel>=150){
                if(this.cleanInterval<10){
                    this.beansCount = this.beansCount-10;
                    this.waterLevel = this.waterLevel-150;
                    this.cleanInterval++;
                    System.out.println("Making a black coffee!");
                }
                else{
                    System.out.println("Run cleaning program before proceeding!");
                }
            }
            else{
                System.out.println("Not enough water!");
            }
        }
        else{}
    }
    
    public void checkStatus(){
        System.out.println("Beans at: " + this.beansCount + " g");
        System.out.println("Waterlevel at: " + this.waterLevel + " ml");
        System.out.println("Uses since last clean: " + this.cleanInterval);
    }
    

}
