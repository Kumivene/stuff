/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Water2;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class WaterDispenser implements Runnable{
    
    int waterLevel;
    volatile boolean keepRunning;
    
    public WaterDispenser(){
        this.waterLevel = 1000;
        this.keepRunning = false;
        
    }
    
    public void run(){
        
        while(true){
            try {
            if(keepRunning && this.waterLevel >= 10){
                
                    dispenseWater();
                    Thread.sleep(1000);
                
            }
            else if(!keepRunning && this.waterLevel >= 10){
                
            }
            else{
                System.out.println("out of water!");
                Thread.sleep(1000);
            }}
            catch (InterruptedException ex) {
                    Logger.getLogger(WaterDispenser.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        
    }
    
    public void dispenseWater(){
        System.out.println("*");
        this.waterLevel = this.waterLevel-100;
        System.out.println(this.waterLevel);
    }
    public void toggleFlow(){
        this.keepRunning = !this.keepRunning;
    }
    
    public void addWater(int wateeeeeer){
        
        this.waterLevel = this.waterLevel + wateeeeeer;
    }
    
    
}
